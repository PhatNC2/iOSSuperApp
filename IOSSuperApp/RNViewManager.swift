//
//  RNViewManager.swift
//  IOSSuperApp
//
//  Created by PhatNC on 19/06/2024.
//

import Foundation
import React

class RNViewManager: NSObject {
    private var bridge: RCTBridge?
    private var useLocalHost: Bool = true
    
    static let sharedInstance = RNViewManager()
    
    func createBridgeIfNeeded(useLocalHost: Bool) -> RCTBridge {
        self.useLocalHost = useLocalHost
        if bridge == nil {
            bridge = RCTBridge(delegate: self, launchOptions: nil)
        }
        return bridge!
    }
    
    func viewForModule(_ moduleName: String, initialProperties: [String : Any]?, useLocalHost: Bool) -> RCTRootView {
        let viewBridge = createBridgeIfNeeded(useLocalHost: useLocalHost)
        let rootView: RCTRootView = RCTRootView(
            bridge: viewBridge,
            moduleName: moduleName,
            initialProperties: initialProperties
        )
        return rootView
    }
    
    func getBridge() -> RCTBridge? {
        return bridge
    }
}

extension RNViewManager: RCTBridgeDelegate {
    func sourceURL(for bridge: RCTBridge!) -> URL! {
        #if DEBUG
        if useLocalHost {
            return URL(string: "http://localhost:8081/index.bundle?platform=ios")
        } else {
            return Bundle.main.url(forResource: "main", withExtension: "jsbundle")
        }
        #else
        return Bundle.main.url(forResource: "main", withExtension: "jsbundle")
        #endif
    }
}
