//
//  TestConnectNativeModule.swift
//  IOSSuperApp
//
//  Created by PhatNC on 20/06/2024.
//

import Foundation
import React

@objc(TestConnectNativeModule)
class TestConnectNativeModule: NSObject {
  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }

  @objc
  func sendMessageToNative(_ rnMessage: String) {
    print("This log is from swift: \(rnMessage)")
  }

  @objc
  func sendCallbackToNative(_ rnCallback: RCTResponseSenderBlock) {
    rnCallback(["A greeting from swift"])
  }

  @objc
  func goToSecondViewController (_ reactTag: NSNumber) {
    // Your implementation for navigation to the second view controller
  }

  @objc
  func dismissViewController (_ reactTag: NSNumber) {
    DispatchQueue.main.async {
      if let bridge = RNViewManager.sharedInstance.getBridge(),
         let view = bridge.uiManager.view(forReactTag: reactTag) {
        let reactNativeVC: UIViewController! = view.reactViewController()
        reactNativeVC.dismiss(animated: true, completion: nil)
      }
    }
  }
}
