//
//  RNViewController.swift
//  IOSSuperApp
//
//  Created by PhatNC on 19/06/2024.
//

import UIKit
import React

class RNViewController: UIViewController {
    var messageFromNative: String = ""
    var useLocalHost: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        loadReactNativeView()
    }

    private func loadReactNativeView() {
        let rootView = RNViewManager.sharedInstance.viewForModule(
            "miniapp1",
            initialProperties: ["message_from_native": messageFromNative],
            useLocalHost: useLocalHost
        )
        rootView.frame = view.bounds
        rootView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(rootView)
    }
}
