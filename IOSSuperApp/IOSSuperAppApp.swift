//
//  IOSSuperAppApp.swift
//  IOSSuperApp
//
//  Created by PhatNC on 16/06/2024.
//

import SwiftUI

@main
struct IOSSuperAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
