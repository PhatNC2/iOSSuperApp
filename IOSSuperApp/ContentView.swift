//
//  ContentView.swift
//  IOSSuperApp
//
//  Created by PhatNC on 16/06/2024.
//

import SwiftUI

struct ContentView: View {
    @State private var message: String = ""
    @State private var showReactNativeView: Bool = false
    @State private var useLocalHost: Bool = true
    
    var body: some View {
        VStack {
            TextField("Enter message", text: $message)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()

            Toggle(isOn: $useLocalHost) {
                Text("Use Metro Server")
            }
            .padding()

            Button(action: {
                showReactNativeView.toggle()
            }) {
                Text("Go to React Native View")
            }
            .padding()
            .fullScreenCover(isPresented: $showReactNativeView) {
                           RNView(messageFromNative: message, useLocalHost: useLocalHost)
                       }
//            .sheet(isPresented: $showReactNativeView) {
//                RNView(messageFromNative: message, useLocalHost: useLocalHost)
//            }
        }
        .padding()
    }
}

struct RNView: UIViewControllerRepresentable {
    var messageFromNative: String
    var useLocalHost: Bool

    func makeUIViewController(context: Context) -> UIViewController {
        let viewController = RNViewController()
        viewController.messageFromNative = messageFromNative
        viewController.useLocalHost = useLocalHost
        return viewController
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        // No need to update anything here
    }
}


#Preview {
    ContentView()
}
